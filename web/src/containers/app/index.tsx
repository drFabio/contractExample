import React, { createContext } from 'react'
import { Main } from '../main'
import { SummaryDescription } from '../../components/contextual/summaryDescription'
import { AppContext } from './types'
import { useAppContext } from './useAppContext'
import { Pages } from '../../types'
import { Success } from '../success'
export const Context = createContext<Partial<AppContext>>({})
const { Provider } = Context

export const testId = "App"

export function App() {

  const contextValue = useAppContext()
  const { page } = contextValue
  return (
    <Provider value={contextValue} >
      <div data-testid={testId} className="mainContainer">
        {page === Pages.MAIN && (
          <Main />
        )}
        {page === Pages.SUCCESS && (
          <Success />
        )}
        <SummaryDescription />
      </div>
    </Provider>
  )
}