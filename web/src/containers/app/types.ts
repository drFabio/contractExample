
import {
  FormOperator,
  Summary,
  AppState,
  Tariff,
  SummaryError,
  Pages
} from 'types'

export type fieldSetter = FormOperator<Summary>
export type AppContext = {
  setField: fieldSetter
  setLoading: (loading: boolean) => void
  setTariffs: (tariffs: Tariff[]) => void,
  setErrors: (errors: SummaryError) => void
  setPage: (page: Pages) => void
} & AppState
