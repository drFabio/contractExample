import React from "react"
import { render, RenderResult } from "@testing-library/react"

jest.mock('../../../hooks/useTariffs')
jest.mock('../../../hooks/useSummary')


import { App, testId } from '../'

describe('<App />', () => {

  function getSut(): RenderResult {
    return render(<App />)
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
})