import { useState } from 'react'
import { AppState, Tariff, SummaryError, Pages } from '../../types'
import { AppContext, fieldSetter } from './types'

/**
 * Creates the app context ans manage state
 */
export function useAppContext() {
  const [state, setState] = useState<AppState>({
    summary: {},
    loadingTarif: true,
    tarifs: [],
    errors: {},
    page: Pages.MAIN
  })
  // utility to safely async set the state
  const setStateSafely = (newState: Partial<AppState>) => setState((oldState) => ({ ...oldState, ...newState }))

  const { summary } = state


  const setField: fieldSetter = (key, value) => {
    setStateSafely({
      summary: { ...summary, [key]: value }
    })
  }
  const setLoading = (loadingTarif: boolean) => {
    setStateSafely({ loadingTarif })
  }
  const setTariffs = (tarifs: Tariff[]) => {
    setStateSafely({ tarifs })
  }
  const setErrors = (errors: SummaryError) => {
    setStateSafely({ errors })
  }
  const setPage = (page: string) => {
    setStateSafely({ page })
  }

  const contextValue: AppContext = {
    ...state,
    setField,
    setLoading,
    setTariffs,
    setErrors,
    setPage
  }
  return contextValue
}