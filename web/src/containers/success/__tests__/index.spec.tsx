import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { Success, testId } from '../'

describe('<Success />', () => {

  function getSut(): RenderResult {
    return render(<Success />)
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    getByTestId(testId)
  })
})