import React, { FC, HTMLAttributes } from 'react'
import { useSetPage } from '../../hooks/useSetPage'
import { Pages } from '../../types'

export const testId = 'Success'
export type Props = HTMLAttributes<HTMLElement>

export const Success: FC<Props> = (props) => {
  const setPage = useSetPage()
  return (
    <section
      data-testid={testId}
      {...props}
    >
      <h3 className="positive huge">Success!</h3>
      <button onClick={() => setPage(Pages.MAIN)}>Go back</button>
    </section>
  )
}