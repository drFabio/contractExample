import React from "react"
import { render, fireEvent } from "@testing-library/react"
import { mocked } from 'ts-jest/utils'
import { testId } from '../'
import { Pages } from "../../../types"

jest.mock('../../../hooks/useSummary')
jest.mock('../../../hooks/useTariffs')
jest.mock('../../../hooks/useErrors')
const mockSetPage = jest.fn()
jest.mock('../../../hooks/useSetPage', () => ({ useSetPage: () => mockSetPage }))
jest.mock('../../../utils/validateSummary')
import { validateSummary } from '../../../utils/validateSummary'


describe('<Main />', () => {
  const { Main } = require('../')
  function getSut() {
    return render(<Main />)
  }
  beforeEach(() => {
    mockSetPage.mockReset()
  })
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
  describe('submit', () => {
    it('goes to success if no errors', () => {
      const mockedValidator = mocked(validateSummary, true)
      mockedValidator.mockImplementationOnce(() => ({}))
      const { getByTestId } = getSut()
      const submit = getByTestId(`${testId}_submit`)
      fireEvent.click(submit)
      expect(mockSetPage).toHaveBeenCalledWith(Pages.SUCCESS)
    })
    it('does not go to success if there are errors', () => {
      const mockedValidator = mocked(validateSummary, true)
      mockedValidator.mockImplementationOnce(() => ({ email: 'error' }))
      const { getByTestId } = getSut()
      const submit = getByTestId(`${testId}_submit`)
      fireEvent.click(submit)
      expect(mockSetPage).not.toHaveBeenCalled()
    })
  })
})