import React, { FC, HTMLAttributes } from 'react'
import { EntryForm } from '../../components/contextual/entryForm'
import { BankDetailsForm } from '../../components/contextual/bankDetailsForm'
import { TariffForm } from '../../components/contextual/tariffForm'
import { Form } from '../../components/common/form'
import { useSummary } from '../../hooks/useSummary'
import { useErrors } from '../../hooks/useErrors'
import { useSetPage } from '../../hooks/useSetPage'
import { Pages } from '../../types'

import { validateSummary } from '../../utils/validateSummary'

export const testId = 'Main'
export type Props = HTMLAttributes<HTMLElement>

export const Main: FC<Props> = (props) => {
  const [summary, setSummary] = useSummary()
  const [errors, setErrors] = useErrors()
  const setPage = useSetPage()
  const handleSubmit = () => {
    const validateErrors = validateSummary(summary)
    const isValid = Object.keys(validateErrors).length === 0
    if (isValid) {
      setPage(Pages.SUCCESS)
    }
    setErrors(validateErrors)

  }
  return (
    <section data-testid={testId} {...props} className="mainContent">
      <Form
        onSubmit={handleSubmit}
      >
        <EntryForm
          value={summary}
          onChange={setSummary}
          errors={errors}
        />
        <TariffForm
          value={summary}
          onChange={setSummary}
          errors={errors}
        />
        <BankDetailsForm
          value={summary}
          onChange={setSummary}
          errors={errors}
        />
        <button
          type="submit"
          data-testid={`${testId}_submit`}
        >
          Send
      </button>
      </Form>
    </section>

  )
}