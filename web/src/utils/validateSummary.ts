import { Summary, FormError } from '../types'


const validatePassword = (value?: any) => (
  !!value &&
  value.length >= 8 &&
  /[A-Z]/.test(value) &&
  /\d/.test(value)
)
const validateIBAN = (value?: any) => {
  return !!value && value.length >= 2 && value.length <= 22
}

const isTrue = (value?: any) => (!!value)

const validateTariff = (value?: any) => value > -1
const validateEmail = (value?: any) => {
  if (typeof value === 'string') {
    return /.+\@.+\..+/.test(value)
  }
  if (!value) return true

  return false
}
export const validators = {
  password: validatePassword,
  iban: validateIBAN,
  confirmPayment: isTrue,
  currentTariff: validateTariff,
  email: validateEmail
}

export const errorMessages = {
  password: 'Needs to have 8 chars with 1 uppercase and 1 number',
  iban: 'Must have between 2 and 22 chars',
  confirmPayment: 'Must be checked',
  currentTariff: 'Select one',
  email: 'Must be an e-mail'
}

/**
 * Performs form validation
 * @param summary
 */
export function validateSummary(summary: Partial<Summary>): Partial<FormError<Summary>> {
  const errors: Partial<FormError<Summary>> = {}
  Object.keys(validators).forEach(key => {
    const value = summary[key] as any
    const validator = validators[key]
    if (!validator(value)) {
      errors[key] = errorMessages[key]
    }
  })
  return errors
}