import { Tariff } from '../types'

/**
 * @param tarif
 * @returns the formatted tarif
 */
export function getTarifText(tarif?: Tariff) {
  if (!tarif) return ''
  return `${tarif.title} ${tarif.price}`
}