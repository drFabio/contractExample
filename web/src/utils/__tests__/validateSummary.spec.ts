import { validateSummary, errorMessages } from '../validateSummary'


const validIban = 'abcdefghijklmnopqrstuv'
const validPassword = 'Passwor3'
const requiredErrorMessages = { ...errorMessages }
delete requiredErrorMessages.email

describe('validateSummary', () => {
  it('Returns required fields errors if summary empty', () => {
    const errors = validateSummary({})
    expect(errors).toEqual(requiredErrorMessages)
  })
  it('Returns no errors if valid sumary', () => {
    const validSummary = {
      iban: validIban,
      password: validPassword,
      confirmPayment: true,
      currentTariff: 1
    }
    const errors = validateSummary(validSummary)
    expect(errors).toEqual({})
  })
  it('Returns email error if invalid email', () => {

    const errors = validateSummary({ email: 'invalid' })
    expect(errors.email).toEqual(errorMessages.email)
  })
  describe('password', () => {
    it('passes', () => {
      const password = validPassword
      const errors = validateSummary({ password })
      expect(errors.password).toBeFalsy()
    })
    it('fails with less than 8 charactes', () => {
      const password = 'Passwor'
      const errors = validateSummary({ password })
      expect(errors.password).toEqual(errorMessages.password)
    })
    it('fails with no number', () => {
      const password = 'Password'
      const errors = validateSummary({ password })
      expect(errors.password).toEqual(errorMessages.password)
    })
    it('fails with no uppercase', () => {
      const password = validPassword.toLowerCase()
      const errors = validateSummary({ password })
      expect(errors.password).toEqual(errorMessages.password)
    })
  })
  it('Confirm payment must be true', () => {
    const errors = validateSummary({ confirmPayment: false })
    expect(errors.confirmPayment).toEqual(errorMessages.confirmPayment)
  })
  it('Tarif must be selected', () => {
    const errors = validateSummary({ currentTariff: -1 })
    expect(errors.currentTariff).toEqual(errorMessages.currentTariff)
  })
  describe('iban', () => {
    it('Must be more than 2 chars', () => {
      const errors = validateSummary({ iban: 'a' })
      expect(errors.iban).toEqual(errorMessages.iban)
    })
    it('Must be more less than 22 chars', () => {
      const errors = validateSummary({ iban: `${validIban}s` })
      expect(errors.iban).toEqual(errorMessages.iban)
    })
    it('Passes with more than 2 chars', () => {
      const errors = validateSummary({ iban: 'abc' })
      expect(errors.iban).toBeFalsy()
    })
    it('Passes with exactly 22 chars', () => {
      const errors = validateSummary({ iban: validIban })
      expect(errors.iban).toBeFalsy()
    })

  })
})