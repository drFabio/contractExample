
/**
 * @param dateText an ISODate
 * @returns a dd/mm/YYYY date
 */
export function getDateText(dateText?: string) {
  if (!dateText) return ''
  const date = new Date(dateText)
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
}