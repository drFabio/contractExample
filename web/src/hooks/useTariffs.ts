import { useEffect, useContext } from 'react'
import axios, { } from 'axios'
import { Tariff } from 'types'
import { Context } from '../containers/app'

const TARIFF_URL = 'https://my-json-server.typicode.com/harsha-vemu/demo/tarifs'


export type Response = [Tariff[], boolean]
/**
 * Hook to get the tariff data and set it on the context
 */
export function useTariffs(): Response {
  const { setLoading, setTariffs, tarifs, loadingTarif } = useContext(Context)

  const fetchTarrifs = async () => {
    const result = await axios(TARIFF_URL)
    setLoading(false)
    if (result.status !== 200) {
      return []
    }
    setTariffs(result.data as Tariff[])
  }

  useEffect(() => {
    fetchTarrifs()
  }, [])
  return [tarifs, loadingTarif]
}