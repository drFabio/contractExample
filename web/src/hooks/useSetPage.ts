import { useContext } from 'react'
import { Context } from '../containers/app'

/**
 * Hook to change pages using the context setter
 */
export function useSetPage() {
  const { setPage } = useContext(Context)
  return setPage
}