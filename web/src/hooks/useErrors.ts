import { useContext } from 'react'
import { Context } from '../containers/app'
import { SummaryError } from '../types'

/**
 * Hook to get error values
 */
export function useErrors(): [SummaryError, (SummaryError) => void] {
  const { errors, setErrors } = useContext(Context)
  return [errors, setErrors]
}