import { useContext } from 'react'
import { Context } from '../containers/app'
import { Summary, FormOperator } from 'types'

/**
 * Hook to get summary values
 */
export function useSummary(): [Partial<Summary>, FormOperator<Summary>] {
  const { summary, setField } = useContext(Context)
  return [summary, setField]
}