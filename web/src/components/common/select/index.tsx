import React, { FC, SelectHTMLAttributes } from 'react'
import { Inputable, Props as InputableProps } from '../inputable'
import { Optionable } from 'types'

export type Props = {
  onChange?: (key: string, value: any) => void,
  options: Optionable[]
} & InputableProps & Omit<SelectHTMLAttributes<HTMLSelectElement>, 'onChange'>

export const testId = 'Select'
/*
* Select with label and error display
*/
export const Select: FC<Props> = ({ onChange, value = '', options, required, ...other }) => {
  const { id, name } = other
  const fieldKey = id || name

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (!onChange) return
    const { value } = e.target
    onChange(fieldKey, value)
  }
  return (
    <Inputable
      data-testid={other['data-testid'] || testId}
      required={required}
      {...other}
    >
      <select
        id={id}
        name={name}
        value={value}
        required={required}
        onChange={handleChange}
        data-testid={`${testId}_select`}
      >
        {options.map(({ id, value: optionValue, title }) => (
          <option
            key={id || optionValue}
            value={optionValue}
          >
            {title}
          </option>
        ))}
      </select>
    </Inputable>
  )
}