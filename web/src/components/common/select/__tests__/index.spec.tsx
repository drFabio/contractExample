import React from "react"
import { render, RenderResult, fireEvent } from "@testing-library/react"
import { Select, Props, testId } from '../'

const mockOptions = [{ id: 'option1', value: 'option1', title: 'option1' }]
describe('<Select />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(
      <Select
        options={mockOptions}
        {...props}
      />
    )
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
  it('returns the value as key,value', () => {
    const id = 'mockId'
    const onChange = jest.fn()
    const { getByTestId } = getSut({ id, onChange })
    const select = getByTestId(`${testId}_select`)
    const newValue = mockOptions[0].value
    fireEvent.change(select, { target: { value: newValue } })
    expect(onChange).toHaveBeenCalledWith(id, newValue)
  })
})