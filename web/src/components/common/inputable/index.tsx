import React, { FC, HTMLAttributes } from 'react'

export type Props = {
  label?: string | null,
  error?: string,
  name?: string,
  id?: string,
  required?: boolean
} & Omit<HTMLAttributes<HTMLElement>, 'onChange'>

export const testId = 'Inputable'
/*
* Inputable with label and error display, used by selects, inputs whatever
*/
export const Inputable: FC<Props> = ({ label, name, id, error, children, required, ...other }) => {
  id = id || name
  if (label !== null) {
    label = label || name || id
  }

  return (
    <div
      data-testid={other['data-testid'] || testId}
      className="inputable"
    >
      {label && (
        <label
          htmlFor={id}
          data-testid={`${testId}_label`}
          className={required ? 'required' : ''}
        >
          {label}
        </label>
      )}
      {error && (
        <div
          data-testid={`${testId}_error`}
          className="error"
        >
          {error}
        </div>
      )}
      {children}

    </div>
  )
}