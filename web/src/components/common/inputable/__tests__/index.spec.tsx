import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { Inputable, Props, testId } from '../'

describe('<Inputable />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(<Inputable {...props} />)
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
  it('shows error', () => {
    const error = 'Some error'
    const { getByTestId, getByText } = getSut({ error })
    const errorTestId = `${testId}_error`
    const errorContainer = getByTestId(errorTestId)
    expect(errorContainer).toBeTruthy
    const errorText = getByText(error)
    expect(errorText).toBeTruthy
  })
})