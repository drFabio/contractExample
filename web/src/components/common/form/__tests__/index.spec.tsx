import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { Form, Props, testId } from '../'

describe('<Form />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(<Form {...props} />)
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
})