import React, { FC, FormHTMLAttributes, FormEvent } from 'react'

export type Props = FormHTMLAttributes<HTMLFormElement> & {
  onSubmit?: () => void
}
export const testId = 'form'
/**
 * Generic Form, noValidate by default and prevents submit
 */
export const Form: FC<Props> = ({ onSubmit, ...props }) => {
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (onSubmit) {
      onSubmit()
    }
  }
  return (<form noValidate data-testid={testId} {...props} onSubmit={handleSubmit} />)
}