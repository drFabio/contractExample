import React from "react"
import { render, RenderResult, fireEvent } from "@testing-library/react"
import { Field, Props, testId } from '../'

describe('<Field />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(<Field {...props} />)
  }
  it('renders', () => {
    const { getByTestId } = getSut()
    const container = getByTestId(testId)
    expect(container).toBeTruthy
  })
  it('returns the value as key,value', () => {
    const id = 'mockId'
    const onChange = jest.fn()
    const { getByTestId } = getSut({ id, onChange })
    const input = getByTestId(`${testId}_input`)
    const newValue = 'newValue'
    fireEvent.change(input, { target: { value: newValue } })
    expect(onChange).toHaveBeenCalledWith(id, newValue)
  })
  it('returns checked if checkbox', () => {
    const id = 'mockId'
    const onChange = jest.fn()
    const checked = true

    const { getByTestId } = getSut({ id, onChange, type: 'checkbox', checked })
    const input = getByTestId(`${testId}_input`)
    fireEvent.click(input)
    expect(onChange).toHaveBeenCalledWith(id, !checked)
  })
})