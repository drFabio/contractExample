import React, { FC, InputHTMLAttributes } from 'react'
import { Inputable, Props as InputableProps } from '../inputable'

export type Props = {
  onChange?: (key: string, value: any) => void
} & InputableProps & Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'>

export const testId = 'Field'
/*
* Field with label and error display
*/
export const Field: FC<Props> = ({ onChange, value = '', type, checked, required, ...other }) => {
  const { id, name } = other
  const fieldKey = id || name
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = e.target
    if (!onChange) return
    // checkbox don't care about value, only checked
    if (type === 'checkbox') {
      onChange(fieldKey, checked)
      return
    }
    onChange(fieldKey, value)
  }
  return (
    <Inputable
      data-testid={other['data-testid'] || testId}
      required={required}
      {...other}
    >
      <input
        id={id}
        name={name}
        value={value}
        type={type}
        checked={checked}
        onChange={handleInputChange}
        data-testid={`${testId}_input`}
        required={required}
      />
    </Inputable>
  )
}