import React from "react"
import { mocked } from 'ts-jest/utils'
import { render, RenderResult } from "@testing-library/react"
import { SummaryDescription, Props, testId } from '../'
import { Summary } from 'types'
jest.mock('../useDescriptionData')
import { useDescriptionData } from '../useDescriptionData'

const defaultValue: Partial<Summary> = {
  firstName: 'firstName',
  email: 'email@email.com',
  lastName: 'lastName',
  password: 'Password1',
  customerNumber: 'customerNumber',
  startDate: '2020-04-15T15:42:20.823Z',
  currentProvider: 'currentProvider',
  currentTariff: 1
}

describe('<SummaryDescription />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    const mockedHook = mocked(useDescriptionData, true)
    const response = mockedHook()
    mockedHook.mockImplementationOnce(() => ({
      ...response,
      summary: defaultValue
    }))
    return render(
      <SummaryDescription
        {...props}
      />)
  }
  it('renders', () => {
    const { getByTestId, getByText } = getSut()
    getByTestId(testId)
    const expectedName = `${defaultValue.firstName} ${defaultValue.lastName}`
    getByText(expectedName)
    getByText(defaultValue.email)
  })
})