import React, { FC, HTMLAttributes } from 'react'
import { useDescriptionData } from './useDescriptionData'

export type Props = HTMLAttributes<HTMLElement>
export const testId = 'summaryDescription'

/**
 * The full summary description
 */
export const SummaryDescription: FC<Props> = (props) => {
  const { summary, tarifLabel, parsedDate } = useDescriptionData()
  const { password, confirmPayment } = summary
  const hasPassword = !!password
  return (
    <section data-testid={testId} {...props} className="summaryContent">
      <h4>Summary</h4>
      <dl>
        <dt>Name</dt>
        <dd>{summary.firstName} {summary.lastName}</dd>
        <dt>Email</dt>
        <dd>{summary.email}</dd>
        <dt>Password</dt>
        <dd className={hasPassword ? 'positive' : 'negative'}>{hasPassword ? 'Present' : 'Absent'}</dd>
      </dl>
      <dl>
        <dt>Tariff</dt>
        <dd>{tarifLabel}</dd>
        <dt>Provider</dt>
        <dd>{summary.currentProvider}</dd>
        <dt>Customer Number</dt>
        <dd>{summary.customerNumber}</dd>
        <dt>Start Date</dt>
        <dd>{parsedDate}</dd>
      </dl>
      <dl>
        <dt>IBAN</dt>
        <dd>{summary.iban}</dd>
        <dt>Confirmed</dt>
        <dd className={confirmPayment ? 'positive' : 'negative'}>{confirmPayment ? 'confirmed' : 'unconfirmed'}</dd>
      </dl>
    </section>
  )
}