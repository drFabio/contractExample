import { useMemo } from 'react'

import { useTariffs } from '../../../hooks/useTariffs'
import { useSummary } from '../../../hooks/useSummary'
import { getTarifText } from '../../../utils/getTarifText'
import { getDateText } from '../../../utils/getDateText'

/**
 * Hook to wrap up the needed data for summary
 */
export function useDescriptionData() {
  const [tariffs, loadingTarifs] = useTariffs()
  const [summary] = useSummary()
  const tarifLabel = useMemo<string>(() => {
    const currentTariff = tariffs.find((t) => t.id === summary.currentTariff)
    return getTarifText(currentTariff)
  }, [summary.currentTariff, loadingTarifs])

  const parsedDate = getDateText(summary.startDate)
  return { summary, tarifLabel, parsedDate }
}