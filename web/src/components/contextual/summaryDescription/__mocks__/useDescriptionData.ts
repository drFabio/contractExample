export const useDescriptionData = jest.fn(() => (
  {
    tarifLabel: 'mockLabel',
    summary: {},
    parsedDate: '01/02/2020'
  }
))