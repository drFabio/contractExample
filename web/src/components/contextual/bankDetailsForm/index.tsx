import React, { FC } from 'react'
import { BankDetails, FormOperator, FormError } from 'types'

import { Field } from '../../common/field'

export type Props = {
  value: Partial<BankDetails>,
  onChange: FormOperator<BankDetails>,
  errors: Partial<FormError<BankDetails>>

}
export const testId = 'bankDetailsForm'
/**
 * Sub form with bank details
 */
export const BankDetailsForm: FC<Props> = ({ value, onChange, errors }) => {
  return (
    <div data-testid={testId} className="subForm">
      <Field
        name={'iban'}
        data-testid={`${testId}iban`}
        label={'IBAN'}
        value={value.iban}
        onChange={onChange}
        maxLength={22}
        minLength={2}
        error={errors.iban}
        required
      />
      <Field
        name={'confirmPayment'}
        data-testid={`${testId}confirmPayment`}
        label={'Confirm payment'}
        checked={!!value.confirmPayment}
        onChange={onChange}
        type={'checkbox'}
        error={errors.confirmPayment}
        required
      />
    </div>
  )
}