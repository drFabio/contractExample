import React, { FC } from 'react'
import {
  TariffDetails,
  FormOperator,
  FormError,
  Optionable
} from 'types'
import { useTariffs } from '../../../hooks/useTariffs'

import { Field } from '../../common/field'
import { Select } from '../../common/select'
import { getTarifText } from '../../../utils/getTarifText'

export type Props = {
  value: Partial<TariffDetails>,
  onChange: FormOperator<TariffDetails>,
  errors: Partial<FormError<TariffDetails>>
}
export const testId = 'tariffForm'

/**
 *
 * @param param0 subfform with tariff data
 */
export const TariffForm: FC<Props> = ({ value, onChange, errors }) => {
  const [tariffs] = useTariffs()
  const options: Optionable[] = tariffs.map((tarif) => ({
    id: tarif.id,
    value: tarif.id,
    title: getTarifText(tarif)
  }))
  options.push({
    value: -1,
    title: 'Select'
  })
  return (
    <div data-testid={testId} className="subForm">
      <Select
        label={'tariff'}
        name={'currentTariff'}
        value={value.currentTariff || -1}
        error={errors.currentTariff}
        options={options}
        required
        onChange={(id, value) => onChange(id as any, parseInt(value, 10))}

      />
      <Field
        name={'currentProvider'}
        data-testid={`${testId}currentProvider`}
        label={'Provider'}
        value={value.currentProvider}
        error={errors.currentProvider}
        onChange={onChange}
      />
      <Field
        name={'customerNumber'}
        data-testid={`${testId}customerNumber`}
        label={'Customer Number'}
        value={value.customerNumber}
        error={errors.customerNumber}
        onChange={onChange}
      />
      <Field
        name={'startDate'}
        data-testid={`${testId}startDate`}
        label={'Start Date'}
        type={'date'}
        value={value.startDate}
        error={errors.startDate}
        onChange={onChange}
      />
    </div>
  )
}