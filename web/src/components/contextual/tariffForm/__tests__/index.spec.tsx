import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { TariffForm, Props, testId } from '../'
import { TariffDetails } from 'types'
jest.mock('../../../../hooks/useTariffs')

const defaultValue: Partial<TariffDetails> = {
  customerNumber: 'customerNumber',
  startDate: '2020-04-15T15:42:20.823Z',
  currentProvider: 'currentProvider',
  currentTariff: 1
}

describe('<TariffForm />', () => {
  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(
      <TariffForm
        onChange={jest.fn()}
        {...props}
        value={props.value || defaultValue}
        errors={{}}
      />)
  }
  it('renders', () => {
    const { getByTestId, getByDisplayValue } = getSut()
    getByTestId(testId)
    getByDisplayValue(defaultValue.currentProvider)
    getByDisplayValue(defaultValue.customerNumber)
  })
})