import React, { FC } from 'react'
import { PersonalDetails, FormOperator, FormError } from 'types'

import { Field } from '../../common/field'

export type Props = {
  value: Partial<PersonalDetails>,
  onChange: FormOperator<PersonalDetails>,
  errors: Partial<FormError<PersonalDetails>>
}
export const testId = 'entryForm'
/**
 * Sub form with personal data
 */
export const EntryForm: FC<Props> = ({ value, onChange, errors }) => {
  return (
    <div data-testid={testId} className="subForm">
      <Field
        name={'firstName'}
        data-testid={`${testId}firstName`}
        label={'Name'}
        value={value.firstName}
        onChange={onChange}
        error={errors.firstName}
      />
      <Field
        name={'lastName'}
        data-testid={`${testId}lastName`}
        label={'Last Name'}
        value={value.lastName}
        onChange={onChange}
        error={errors.lastName}
      />
      <Field
        name={'email'}
        data-testid={`${testId}email`}
        label={'E-mail'}
        value={value.email}
        onChange={onChange}
        error={errors.email}
      />
      <Field
        name={'password'}
        data-testid={`${testId}password`}
        label={'Password'}
        type={'password'} value={value.password}
        onChange={onChange}
        error={errors.password}
        required
      />
    </div>
  )
}