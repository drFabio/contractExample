import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { EntryForm, Props, testId } from '../'
import { PersonalDetails } from 'types'

const defaultValue: PersonalDetails = {
  firstName: 'firstName',
  email: 'email@email.com',
  lastName: 'lastName',
  password: 'Password1',
}

describe('<EntryForm />', () => {

  function getSut(props: Partial<Props> = {}): RenderResult {
    return render(
      <EntryForm
        onChange={jest.fn()}
        {...props}
        value={props.value || defaultValue}
        errors={{}}
      />
    )
  }
  it('renders', () => {
    const { getByTestId, getByDisplayValue } = getSut()
    const container = getByTestId(testId)
    const desiredFields = [
      'firstName',
      'lastName',
      'password',
      'email'
    ]
    expect(container).toBeTruthy
    desiredFields.forEach((field) => {
      const renderedField = getByTestId(`${testId}${field}`)
      expect(renderedField).toBeTruthy
      const element = getByDisplayValue(defaultValue[field])
      expect(element).toBeTruthy
    })
  })
})