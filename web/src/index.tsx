import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './containers/app';
import './global.scss'

ReactDOM.render(<App />, document.getElementById('app'));