export type PersonalDetails = {
  firstName: string
  email: string
  lastName: string
  password: string
}

export type TariffDetails = {
  customerNumber: string
  startDate: string
  currentProvider: string
  currentTariff: number
}

export type BankDetails = {
  iban: string
  confirmPayment: boolean
}
export type Summary = PersonalDetails &
  TariffDetails &
  BankDetails

export type FormError<T> = {
  [key in keyof T]?: string
}

export type FormOperator<T> = (key: keyof T, value: any) => void

export type Tariff = {
  id: number,
  title: string,
  price: number
}

export type Optionable = {
  id?: string | number,
  value: string | number,
  title: string
}

export type SummaryError = Partial<FormError<Summary>>

export enum Pages {
  MAIN = 'main',
  SUCCESS = 'success'
}

export type AppState = {
  summary: Partial<Summary>,
  tarifs: Tariff[],
  loadingTarif: boolean,
  errors: SummaryError,
  page: string
}