# Run with docker
```shell
docker-compose up
```
The site will be on localhost:3001

## Running tests

```node
cd web
npm run test
```

## Rules that were applied
 * password is required
 * IBAN can have 2-22 chars and is required
 * Tariff is required
 * payment confirmation is required

## Tooling

* Typescript - For type safety
* react testing library - For testing
* axios - for requests
* babel - transpiling and es6 support
* webpack - bundling